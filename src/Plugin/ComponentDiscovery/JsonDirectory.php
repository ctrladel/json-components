<?php

namespace Drupal\json_blocks\Plugin\ComponentDiscovery;

use Drupal\Component\Discovery\DiscoveryException;
use Drupal\Component\FileCache\FileCacheFactory;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\sdc\ComponentDiscoveryBase;
use Drupal\sdc\ExtensionType;
use Drupal\sdc\Plugin\Discovery\RegexRecursiveFilterIterator;
use Drupal\sdc\Utilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Discover directories that contain a specific metadata file.
 */

/**
 * @\Drupal\sdc\Annotation\ComponentDiscovery(
 *   id="json_directory",
 *   label=@Translation("YAML Directory Discovery"),
 *   plugin_class="\Drupal\json_blocks\JsonComponent",
 *   weight=0
 * )
 */
class JsonDirectory extends ComponentDiscoveryBase implements ContainerFactoryPluginInterface {

  /**
   * Defines the key in the discovered data where the file path is stored.
   */
  const FILE_KEY = '_discovered_file_path';

  /**
   * An array of directories to scan, keyed by the provider.
   *
   * The value can either be a string or an array of strings. The string values
   * should be the path of a directory to scan.
   *
   * @var array
   */
  protected array $directories = [];

  /**
   * The suffix for the file cache key.
   *
   * @var string
   */
  protected string $fileCacheKeySuffix;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Constructs a YamlDirectoryDiscovery object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler, FileSystemInterface $file_system) {
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $theme_handler;
    $this->fileSystem = $file_system;

    $this->fileCacheKeySuffix = 'sdc_json';
    $this->directories = $this->getScanDirectories();

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('theme_handler'),
      $container->get('file_system')
    );
  }

  /**
   * Get the list of directories to scan.
   *
   * @return string[]
   *   The directories.
   */
  private function getScanDirectories(): array {
    $extension_directories = [
      ...$this->moduleHandler->getModuleDirectories(),
      ...$this->themeHandler->getThemeDirectories(),
    ];
    return array_map(
      static fn(string $path) => rtrim(sprintf(
        '%s%s%s',
        rtrim($path, DIRECTORY_SEPARATOR),
        DIRECTORY_SEPARATOR,
        'components'
      ), DIRECTORY_SEPARATOR),
      $extension_directories
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $plugins = $this->findAll();

    // Flatten definitions into what's expected from plugins.
    $json_definitions = [];
    foreach ($plugins as $provider => $list) {
      foreach ($list as $id => $definition) {
        // Add ID and provider.
        $json_definitions[$id] = $definition + [
            'provider' => $provider,
            'id' => $id,
          ];
      }
    }

    $definitions = [];
    foreach ($json_definitions as $definition) {
      $this->processDefinition($definition);
      $definitions[$definition['id']] = $definition;
    }

    return $definitions;
  }

  /**
   * Gets an iterator to loop over the files in the provided directory.
   *
   * This method exists so that it is easy to replace this functionality in a
   * class that extends this one. For example, it could be used to make the scan
   * recursive.
   *
   * @param string $directory
   *   The directory to scan.
   *
   * @return \Traversable
   *   An \Traversable object or array where the values are \SplFileInfo
   *   objects.
   */
  protected function getDirectoryIterator($directory) {
    // Use FilesystemIterator to not iterate over the . and .. directories.
    $flags = \FilesystemIterator::KEY_AS_PATHNAME
      | \FilesystemIterator::CURRENT_AS_FILEINFO
      | \FilesystemIterator::SKIP_DOTS;
    $directory_iterator = new \RecursiveDirectoryIterator($directory, $flags);
    // Detect "my_component.component.json".
    $regex = '/^([a-z0-9_-])+.component.json/i';
    $filter = new RegexRecursiveFilterIterator($directory_iterator, $regex);
    return new \RecursiveIteratorIterator($filter, \RecursiveIteratorIterator::LEAVES_ONLY, $flags);
  }

  /**
   * {@inheritdoc}
   *
   * The IDs can collide in two different scenarios:
   *
   * 1. Because one component is overriding another one via "weight".
   * 2. Because the same component exists in different themes.
   */
  protected function getIdentifier($file, array $data) {
    $id = $this->fileSystem->basename($file, '.component.json');
    $provider_paths = array_flip($this->directories);
    $provider = $this->findProvider($file, $provider_paths);
    // We use the provider to dedupe components because it does not make sense
    // for a single provider to fork itself.
    return sprintf('%s:%s', $provider, $id);
  }

  /**
   * Finds the provider of the discovered file.
   *
   * The approach here is suboptimal because the provider is actually set in
   * the plugin definition after the getIdentifier is called. So we either do
   * this, or we forego the base class.
   *
   * @param string $file
   *   The discovered file.
   * @param array $provider_paths
   *   The associative array of the path to the provider.
   *
   * @return string
   *   The provider
   */
  private function findProvider(string $file, array $provider_paths): string {
    $parts = explode(DIRECTORY_SEPARATOR, $file);
    array_pop($parts);
    if (empty($parts)) {
      return '';
    }
    $provider = $provider_paths[implode(DIRECTORY_SEPARATOR, $parts)] ?? '';
    return empty($provider)
      ? $this->findProvider(implode(DIRECTORY_SEPARATOR, $parts), $provider_paths)
      : $provider;
  }

  protected function processDefinition(array &$definition) {
    $definition['extension_type'] = $this->moduleHandler->moduleExists($definition['provider'] ?? '')
      ? ExtensionType::Module
      : ExtensionType::Theme;
    $definition['metadata_path'] = $definition[self::FILE_KEY];
    $definition['path'] = $this->fileSystem->dirname($definition['metadata_path']);
    [,$definition['machine_name']] = explode(':', $definition['id'] ?? '');

    // Discover the template.
    $template = Utilities::findAsset($definition['path'], $definition['machine_name'], 'twig', FALSE);
    $definition['template'] = basename($template);
    $definition['documentation'] = 'No documentation found. Add a README.md in your component directory and install the package "league/commonmark" using composer.';
    $documentation_path = sprintf('%s/README.md', $this->fileSystem->dirname($definition['metadata_path']));
    if (class_exists('\League\CommonMark\CommonMarkConverter') && file_exists($documentation_path)) {
      $documentation_md = file_get_contents($documentation_path);
      // phpcs:ignore Drupal.Classes.FullyQualifiedNamespace.UseStatementMissing
      $converter = new \League\CommonMark\CommonMarkConverter();
      $definition['documentation'] = $converter->convert($documentation_md)
        ->getContent();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function findAll() {
    $all = [];

    $files = $this->findFiles();

    $file_cache = FileCacheFactory::get('json_discovery:' . $this->fileCacheKeySuffix);

    // Try to load from the file cache first.
    foreach ($file_cache->getMultiple(array_keys($files)) as $file => $data) {
      $all[$files[$file]][$this->getIdentifier($file, $data)] = $data;
      unset($files[$file]);
    }

    // If there are files left that were not returned from the cache, load and
    // parse them now. This list was flipped above and is keyed by filename.
    if ($files) {
      foreach ($files as $file => $provider) {
        // If a file is empty or its contents are commented out, return an empty
        // array instead of NULL for type consistency.
        try {
          $data = json_decode(file_get_contents($file), TRUE) ?: [];
        }
        catch (InvalidDataTypeException $e) {
          throw new DiscoveryException("The $file contains invalid JSON", 0, $e);
        }
        $data[static::FILE_KEY] = $file;
        $all[$provider][$this->getIdentifier($file, $data)] = $data;
        $file_cache->set($file, $data);
      }
    }

    return $all;
  }

  /**
   * Returns an array of providers keyed by file path.
   *
   * @return array
   *   An array of providers keyed by file path.
   */
  protected function findFiles() {
    $file_list = [];
    foreach ($this->directories as $provider => $directories) {
      $directories = (array) $directories;
      foreach ($directories as $directory) {
        if (is_dir($directory)) {
          /** @var \SplFileInfo $fileInfo */
          foreach ($this->getDirectoryIterator($directory) as $fileInfo) {
            $file_list[$fileInfo->getPathname()] = $provider;
          }
        }
      }
    }
    return $file_list;
  }

}
